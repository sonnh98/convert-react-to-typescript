import { fork, put, takeLatest, race, take } from 'redux-saga/effects';
import { authActionTypes } from './constants';
import { postUnauthed, getAuthed, postAuthed, deleteAuthed } from '../request/actions';
import NavigationService from '../../core/navigation';

const transformLoginAPIError = payload => {
  return payload.message || '';
};

// handlers (workers)
function* handleLogin(action) {
  const { login, password } = action.payload;
  const formData = { login, password };
  yield put(
    postUnauthed({
      uri: '/sign_in',
      successAction: authActionTypes.LOGIN_SUCCESS,
      failureAction: authActionTypes.LOGIN_FAILURE,
      data: { formData }
    })
  );

  const { success, fail } = yield race({
    success: take(authActionTypes.LOGIN_SUCCESS),
    fail: take(authActionTypes.LOGIN_FAILURE)
  });

  if (success) {
    NavigationService.goBack();
  } else if (fail) {
    const errMsg = transformLoginAPIError(fail.payload);
    yield put({
      type: authActionTypes.LOGIN_FAILURE,
      payload: {
        data: {
          errorMsg: errMsg
        }
      }
    });
  }
}

function* handleFetchUser() {
  yield put(
    getAuthed({
      uri: 'users/me',
      successAction: authActionTypes.FETCH_USER_SUCCESS,
      failureAction: authActionTypes.FETCH_USER_FAILURE
    })
  );
}

function* handleUpdateUser(action) {
  const { payload } = action;
  const { name, avatar } = payload;
  let formData = {};
  if (name) {
    formData.nickname = name;
  } else if (avatar) {
    formData = new FormData();
    formData.append('avatar', {
      uri: avatar,
      name: Date.parse(new Date()) + '.jpg',
      type: 'multipart/form-data'
    });
  }

  yield put(
    postAuthed({
      uri: 'users/update',
      successAction: authActionTypes.UPDATE_USER_SUCCESS,
      failureAction: authActionTypes.UPDATE_USER_FAILURE,
      data: { formData, isFileUpload: !!avatar }
    })
  );
}

function* handleLogOutUser() {
  yield put(
    deleteAuthed({
      uri: 'log_out',
      successAction: authActionTypes.LOG_OUT_USER_SUCCESS,
      failureAction: authActionTypes.LOG_OUT_USER_FAILURE
    })
  );

  yield put({
    type: authActionTypes.CLEAN_USER_PROFILE
  });
}

// watchers
function* watchLogin() {
  yield takeLatest(authActionTypes.LOGIN_USER, handleLogin);
}

function* watchFetchUser() {
  yield takeLatest(authActionTypes.FETCH_USER, handleFetchUser);
}

function* watchUpdateUser() {
  yield takeLatest(authActionTypes.UPDATE_USER, handleUpdateUser);
}

function* watchLogOut() {
  yield takeLatest(authActionTypes.LOG_OUT_USER, handleLogOutUser);
}

const sagas = [fork(watchLogin), fork(watchFetchUser), fork(watchUpdateUser), fork(watchLogOut)];
export default sagas;
