import { authActionTypes } from './constants';

export const setUserProfile = authObject => ({
  type: authActionTypes.SET_USER_PROFILE,
  payload: { data: { ...authObject } }
});

export const cleanUserProfile = () => ({ type: authActionTypes.CLEAN_USER_PROFILE });
export const logOutUser = () => ({ type: authActionTypes.LOG_OUT_USER });

export const loginUser = ({ login, password }) => ({
  type: authActionTypes.LOGIN_USER,
  payload: { login, password }
});

export const fetchUser = () => ({
  type: authActionTypes.FETCH_USER
});

export const updateUser = ({ avatar, name }) => ({
  type: authActionTypes.UPDATE_USER,
  payload: { avatar, name }
});

export const resetUpload = () => ({
  type: authActionTypes.RESET_UPLOAD
});

export const clearLoginFailure = () => ({
  type: authActionTypes.CLEAR_LOGIN_FAILURE
});
