import {FormActionTypes} from './constants'
import {RequestActionTypes} from '../request/constants'
export const initialState = {
       page_action:'',

       result_data:[],
        answer_data:{},
       current_index:0,
       AHVVAData:'',
        form_length:'',
       request_data:[],
       password_alert:false,
       isReady: false,
    submissionUrl: '',
    backgroundColor:'',
    report_type:''
};

export const reducer = (state = initialState, action) => {
  let data = {};
    let current_index
  data = action.payload;
  console.log(data);
  console.log(action.type);
  switch (action.type) {
    
      case FormActionTypes.GET_MAIN_PAGE_SUCCESS:
          let stage_data = state.request_data;
          stage_data.push(data.views)
          return {...state, request_data: stage_data,current_index:0,form_length:data.form_length,submissionUrl:data.submissionUrl,password_alert:false,backgroundColor:data.backgroundColor,report_type:data.report_type}
      case FormActionTypes.GET_MAIN_PAGE_FAILED:
          return {...state, request_data: [],isReady:false}
      case FormActionTypes.ADD_PAGE_ANSWER:
          let answer = state.answer_data;
          // answer.push(data.answer);
          answer[data.key] = {key:data.key,answer:data.answer,question_text:data.question_text}
          return {...state,answer_data:answer}
      case FormActionTypes.GET_NEXT_PAGE:
          return {...state,isReady:true}
      case FormActionTypes.GET_NEXT_PAGE_SUCCESS:
          let next_stage_data = state.request_data;
          // next_stage_data.push(data.views)
           current_index = state.current_index
          next_stage_data[current_index+1] = data.views
          return {...state, request_data: next_stage_data,current_index:current_index+1,isReady:true,password_alert:false}
      case FormActionTypes.GO_BACK_TO_PRE:
          // let pre_answer = state.answer_data;
          // let pre_re = state.request_data.pop()
          let pre_stage = state.request_data;
          current_index = state.current_index
          return {...state, request_data:pre_stage,current_index:current_index-1}
      case FormActionTypes.GO_BACK_TO_ANY:
          return {...state, current_index:data.index}
      case FormActionTypes.UNMOUNT_FROM:
          return {...state,page_action:'',
       result_data:[],
        answer_data:{},
       current_index:0,
        AHVVAData:'',
        form_length:'',
       request_data:[],
        password_alert:false,
       isReady: false,
        submissionUrl:'',
        report_type:''}
      case RequestActionTypes.REQUEST_COMPLETED:
          return {...state}
      case FormActionTypes.ADD_AHVVA_RESULT:
          return {...state,AHVVAData:data}
      case FormActionTypes.CLEAR_AHVVA_RESULT:
          return {...state,AHVVAData:''}
      case FormActionTypes.PASSWORD_ALERT:
          return {...state,password_alert:true}
      case FormActionTypes.CLEAR_PASSWORD_ALERT:
          return {...state,password_alert:false}
      case FormActionTypes.RELOAD_PROGRESS:
          return {...state,answer_data:data.data, isReady:true,}

  default:
        return state;


  }
};

export default reducer;
