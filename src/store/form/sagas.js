import {call, fork, put, takeLatest, race, take ,cancel,takeEvery} from 'redux-saga/effects';
import {FormActionTypes} from './constants'
import {getAuthed,getUnAuthed} from "../request/actions";

function* fetchMainPage(action) {
    const  successAction  = FormActionTypes.GET_MAIN_PAGE_SUCCESS;
    const  failureAction = FormActionTypes.GET_MAIN_PAGE_FAILED;
    const  passwordAlert  = FormActionTypes.PASSWORD_ALERT;
    const reloadAction ={
  type: FormActionTypes.GO_RELOAD,
  payload: {'password':action.payload.password||'' ,'form_id':action.payload.form_id,'session_id':action.payload.session_id,'index':'0'}
}
  yield put(
    getUnAuthed({
      uri: '/page',
      successAction,
      failureAction,
      data: {queryParams:{'password':action.payload.password||'' ,'form_id':action.payload.form_id,'current_status':'published','index':'0'}}
    })
  );
   const { success, fail } = yield race({
    success: take(successAction),
    fail: take(failureAction)
  });

  if (success) {
      console.log('sss')
    yield  put(reloadAction)
    // console.log(success.payload)

  } else if (fail) {


      console.log(fail.payload)
      if(fail.payload.errorCode == 401){
         if( fail.payload.extraInfo.queryParams.password){
             alert(fail.payload.message)
         }
          yield put({ type: passwordAlert })
      }
  else {
      alert(fail.payload.message)
      window.document.location.pathname = '/'
      }


  }
}
function* reLoadPage(action) {
    const  successAction  = FormActionTypes.RELOAD_PROGRESS;
    const  failureAction = FormActionTypes.FETCH_RELOAD_FAILED;
    const  passwordAlert  = FormActionTypes.PASSWORD_ALERT;
  yield put(
    getUnAuthed({
      uri: '/form_session',
      successAction,
      failureAction,
      data: {queryParams:{'password':action.payload.password||'' ,'form_id':action.payload.form_id,'session_id':action.payload.session_id||''}}
    })
  );
   const { success, fail } = yield race({
    success: take(successAction),
    fail: take(failureAction)
  });

  if (success) {
    // console.log(success.payload)

  } else if (fail) {


      // console.log(fail.payload)
      if(fail.payload.errorCode == 401){
         // if( fail.payload.extraInfo.queryParams.password){
         //     alert(fail.payload.message)
         // }
         //  yield put({ type: passwordAlert })
      }
  else {
      alert(fail.payload.message)
      // window.document.location.pathname = '/'
      }


  }
}
function* fetchNextPage(action) {
    const  successAction  = FormActionTypes.GET_NEXT_PAGE_SUCCESS;
    const  failureAction = FormActionTypes.GET_NEXT_PAGE_FAILED;
    const  passwordAlert  = FormActionTypes.PASSWORD_ALERT;
  yield put(
    getUnAuthed({
      uri: '/page',
      successAction,
      failureAction,
      data: {queryParams:{'password':action.payload.password||'' ,'form_id':action.payload.form_id,'current_status':'published','index':action.payload.index}}
    })
  );
   const { success, fail } = yield race({
    success: take(successAction),
    fail: take(failureAction)
  });

  if (success) {
    // console.log(success.payload)

  } else if (fail) {
      console.log(fail.payload)
      alert(fail.payload.message)
     if(fail.payload.errorCode == 401){
          yield put({ type: passwordAlert })
      }
  else {
      window.document.location.pathname = '/'
      }
  }
}

function* fetchForm(action) {
    const  successAction  = FormActionTypes.GET_NEXT_PAGE_SUCCESS;
    const  failureAction = FormActionTypes.GET_NEXT_PAGE_FAILED;
  yield put(
    getUnAuthed({
      uri: '/search_form',
      successAction,
      failureAction,
      data: {queryParams:{'password':action.payload.password||'' ,'form_id':action.payload.form_id,'company_name':action.payload.companyName}}
    })
  );
   const { success, fail } = yield race({
    success: take(successAction),
    fail: take(failureAction)
  });

  if (success) {
    // console.log(success.payload)

  } else if (fail) {
     alert(fail.payload.message)
    window.document.location.pathname = '/'
  }
}
function* watchForm() {
  yield takeLatest(FormActionTypes.GET_MAIN_PAGE, fetchMainPage);
  yield takeLatest(FormActionTypes.GET_NEXT_PAGE, fetchNextPage);
  yield takeLatest(FormActionTypes.GET_FORM, fetchForm);
  yield takeLatest(FormActionTypes.GO_RELOAD, reLoadPage);
}

const Formsaga = [
  fork(watchForm),
];
export default Formsaga;