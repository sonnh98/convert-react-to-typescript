import {FormActionTypes} from './constants'

export const fetchMainPageData = ({password,form_id,session_id}) => ({
  type: FormActionTypes.GET_MAIN_PAGE,
  payload: {
    password,form_id,session_id
  }
});
export const searchForm = ({password,companyName}) => ({
  type: FormActionTypes.GET_FORM,
  payload: {
    password,companyName
  }
});

export const fetchNextPageData = ({ password,form_id,index }) => ({
  type: FormActionTypes.GET_NEXT_PAGE,
  payload: {
    password,form_id,index
  }
});
export const addPageData = ({  key,answer,question_text}) => ({
  type: FormActionTypes.ADD_PAGE_ANSWER,
  payload: {
key,answer,question_text
  }
});
export const goBackToPrevious = () => ({
  type: FormActionTypes.GO_BACK_TO_PRE,
  payload: {

  }
});
export const goBackToANY = (index) => ({
  type: FormActionTypes.GO_BACK_TO_ANY,
  payload: {
    index
  }
});
export const unmountForm = () => ({
  type: FormActionTypes.UNMOUNT_FROM,
  payload: {

  }
});
export const addAhvvaResult = (data) => ({
  type: FormActionTypes.ADD_AHVVA_RESULT,
  payload: {
data
  }
});
export const reloadProgress = (data) => ({
  type: FormActionTypes.RELOAD_PROGRESS,
  payload: {
data
  }
});
export const passwordAlert = () => ({
  type: FormActionTypes.PASSWORD_ALERT,
  payload: {

  }
});
export const clearpasswordAlert = () => ({
  type: FormActionTypes.CLEAR_PASSWORD_ALERT,
  payload: {

  }
});
export const clearAhvvaResult = (data) => ({
  type: FormActionTypes.CLEAR_AHVVA_RESULT,
  payload: {
  }
});
export const fetchAhvvaResult = (bodyData) => ({
  type: FormActionTypes.GET_AHVVA_RESULT,
  payload: {
bodyData
  }
});
export const fetchAhvvaResultSuccess = (bodyData) => ({
  type: FormActionTypes.GET_AHVVA_RESULT_SUCCESS,
  payload: {
bodyData
  }
});
export const fetchAhvvaResultFailed = (bodyData) => ({
  type: FormActionTypes.GET_AHVVA_RESULT_FAILED,
  payload: {
bodyData
  }
});