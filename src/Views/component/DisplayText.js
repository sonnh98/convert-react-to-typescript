import React from "react";
import { withStyles } from "@material-ui/core/styles/index";
const styles = theme => ({
  Button: {
    color: "#2196F3",
    backgroundColor: "#444444",
    "&:hover": {
      backgroundColor: "#2f2f2f"
    }
  },
  details: {
    alignItems: "center",
    width: "100%"
  },
  table: {
    width: "1000"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
});

class DisplayText extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    try {
      return (
        <div>
          <div className={"subpage"}>
            <div style={{ width: "100%" }}>
              <h1 style={{ whiteSpace: "pre-line" }}>
                {`${this.props.data.title}   Page:${this.props.page_index}`}
              </h1>

              <br />

              <div style={{ boxSizing: "unset", width: "100%" }}>
                <div style={{ height: "230px" }}>
                  <h2 style={{ textAlign: "left" }}>
                    <strong>Consequence</strong>
                  </h2>
                  <h4 style={{ textAlign: "left" }}>
                    {this.props.data.consequence_text}
                  </h4>
                </div>
                <div style={{ height: "300px" }}>
                  <h2 style={{ textAlign: "left" }}>
                    <strong>Likelihood</strong>
                  </h2>
                  <h4 style={{ textAlign: "left" }}>
                    {this.props.data.likelihood_text}
                  </h4>
                </div>
                <div style={{ height: "270px" }}>
                  <h2 style={{ textAlign: "left" }}>
                    <strong>Overall risk</strong>
                  </h2>
                  <h4 style={{ textAlign: "left" }}>
                    {this.props.data.overall_text}
                  </h4>
                </div>

                <div style={{ height: "300px" }}>
                  <h1 style={{ textAlign: "left" }}>
                    <strong>Recommendation</strong>
                  </h1>
                  <h4 style={{ textAlign: "left" }}>
                    {this.props.data.recommendation_text}
                  </h4>
                </div>
              </div>
            </div>
          </div>

          <div style={{ pageBreakAfter: "always" }}></div>
        </div>
      );
    } catch (e) {
      return <p>Loading...</p>;
    }
  }
}

export default withStyles(styles)(DisplayText);
