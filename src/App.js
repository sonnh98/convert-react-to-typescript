import React, { Component } from "react";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store/rootStore";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";
import AhvvaResult from "./Views/AhvvaResult";
import AhvvaPage from "./Views/AhvvaPage";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Theme } from "vardogyir-ui";


class App extends Component {

  createAppBar = () => {
    return (
      <nav
        id="main-header"
        style={{ backgroundColor: "white", zIndex: "-100" }}
        className="navbar navbar-expand-lg navbar-light fixed-top text-right "
      >
        <div
          className="container"
          style={{ maxWidth: 1500, textAlign: "right" }}
        >
          <a className="navbar-brand" href="https://www.vardogyir.com/">
            <img src="/images/title-logo.png" width="40" alt="" />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a className="nav-link" href="/form" style={{ color: "white" }}>
                  Search for a form <span className="sr-only">(current)</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  };

  createFooter = () => {
    return (
      <div className="footer" style={{ backgroundColor: "rgb(44,44,44)" }}>
        <Toolbar>
        </Toolbar>
      </div>
    );
  };

  render() {
    return (
      <Provider store={store}>
        <Router>
          <MuiThemeProvider theme={Theme}>
            <div className="App">
              {this.createAppBar()}
              <Switch>
                <Route exact path="/" />
                <Route path="/AhvvaResult/:req_id" component={AhvvaResult} />
                <Route path="/AhvvaPage/:req_id" component={AhvvaPage} />
              </Switch>
            </div>
          </MuiThemeProvider>
        </Router>
      </Provider>
    );
  }
}

export default App;
