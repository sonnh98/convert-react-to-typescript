const config = {
  VERSION: "0.0.1",
  API_ADDRESS:
    "https://awe2dvmr2b.execute-api.ap-southeast-2.amazonaws.com/dev/form",
  REGION: process.env.REGION || "ap-southeast-2",
  USER_POOL_ID: process.env.USER_POOL_ID || "ap-southeast-2_mQ7Hz6dUm",
  APP_CLIENT_ID: process.env.APP_CLIENT_ID || "546ivvtvkrg9pfo18u08g1j3o2",
  DOMAIN: process.env.DOMAIN || "form.vardogyir.com",
  ISS: "https://cognito-idp.ap-southeast-2.amazonaws.com/ap-southeast-2_mQ7Hz6dUm"
};

export default config;
